#!/bin/sh
#
# Pale Moon Automatic Install for Linux -- Personal PURGE -- File can be used independently.

# Uninstalling Pale Moon notice
printf -- '\n%s\n' " Per your request. Now purging your personal install of Pale Moon.";

# Small delay to give user time to read the above notice.
sleep 3;

# Installation
rm -r -f  "$HOME"/PaleMoon/ ;

# Menu shortcuts
rm -r -f "$HOME"/.local/share/applications/Pale_Moon.desktop ;

# determine the XDG_DESKTOP_DIR, for multi language support!
DESKTOP_DIR=$(xdg-user-dir DESKTOP)
# Current desktop shortcuts
rm -r -f "$DESKTOP_DIR"/Pale_Moon.desktop ;

## PURGE - Everything goes, bye-bye.

# ALL your file cache.
rm -r -f "$HOME/.cache/moonchild productions/" ;

# All your configuration and profile files.
rm -r -f "$HOME/.moonchild productions/" ;

# Exit notice.
printf -- '%s\n' "" "" "" " Thank you for using Pale Moon." \
" Pale Moon, along with all user data, have been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon" "" ""

# exit
exit 0
