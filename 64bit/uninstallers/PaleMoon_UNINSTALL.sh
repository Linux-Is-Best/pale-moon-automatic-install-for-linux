#!/bin/sh
#
# Pale Moon Automatic Install for Linux -- System Uninstall 64-bit -- File can be used independently.

# Uninstalling Pale Moon notice
printf -- '\n%s\n' " Per your request. Now uninstalling Pale Moon.";

# Small delay to give user time to read the above notice.
sleep 3;

# Installation
sudo rm -r -f  /opt/Moonchild_Productions_PaleMoon/ ;

# Menu shortcuts
sudo rm -r -f /usr/share/applications/Pale_Moon.desktop ;

sudo rm -r -f /etc/skel/Desktop/Pale_Moon.desktop ;

# determine the XDG_DESKTOP_DIR, then the DESKTOP_NAME for multi language support, this assumes, that every user has the same locale or Desktop Name
DESKTOP_NAME=$(basename "$(sudo xdg-user-dir DESKTOP)")
# Current deskop shortcuts
sudo rm -r -f /home/*/"$DESKTOP_NAME"/Pale_Moon.desktop ;


# Exit notice.
printf -- '%s\n' "" "" "" " Thank you for using Pale Moon." \
" Pale Moon has been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon." "" ""

# exit
exit 0
