#!/bin/sh
#
# Pale Moon Automatic Install for Linux -- System PURGE 64-bit -- File can be used independently.

# Purge - To remove all copies of Pale Moon installed by this development
#         BUT will also remove ALL cache and configuration files for 
#         ALL copies of Pale Moon for ALL users on your computer. 

# Uninstalling Pale Moon notice
clear ;
printf -- '\n%s\n' " Per your request. Now purging .";

# Small delay to give user time to read the above notice.
sleep 3;

# Installation
sudo rm -r -f  /opt/Moonchild_Productions_PaleMoon/ ;

# Menu shortcuts
sudo rm -r -f /usr/share/applications/Pale_Moon.desktop ;

sudo rm -r -f /etc/skel/Desktop/Pale_Moon.desktop ;

# determine the XDG_DESKTOP_DIR, then the DESKTOP_NAME for multi language support, this assumes, that every user has the same locale or Desktop Name
DESKTOP_NAME=$(basename "$(sudo xdg-user-dir DESKTOP)")
# Current deskop shortcuts
sudo rm -r -f /home/*/"$DESKTOP_NAME"/Pale_Moon.desktop ;

## PURGE - Everything goes, bye-bye.

# ALL your file cache.
sudo rm -r -f "/home/*/.cache/moonchild productions/" ;

# All your configuration and profile files.
sudo rm -r -f "/home/*/.moonchild productions/" ;

# Exit notice.
printf -- '%s\n' "" "" "" " Thank you for using Pale Moon." \
" Pale Moon, along with all user data, have been deleted and uninstalled. Per your request." \
" Really sorry to see you go. Hope to see you again real soon" "" ""

# exit
exit 0
